﻿#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter the text: ";
    std::string text;
    std::getline(std::cin, text);


    if (text.empty())
        std::cout << "String is empty! \n";
    else
    {
        std::cout << "Your text: " << text << "\n" << "String length: " << text.length() << "\n";

        size_t start{ text.find_first_of(text) };
        size_t end{ text.find_last_of(text) };

        std::cout << "First sign: " << text[start] << "\n" << "Last sign: " << text[end] << "\n";
    }


    return 0;
}
